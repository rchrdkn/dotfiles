# gotham.zsh-theme
# Repo: https://bitbucket.org/_richardkeen/.dotfiles

if [ $UID -eq 0 ]; then NCOLOR="red"; else NCOLOR="green"; fi

# color vars
eval my_gray='$FG[242]'
eval my_light_gray='$FG[248]'
eval my_orange='$FG[214]'
eval my_green='%{$fg[green]%}'

collapse_pwd () {
  echo $(pwd | sed -e "s,^$HOME,~," -e "s,^\(.*\)Projects/\([^/]*\),\2,")
}

right_prompt () {
  local git_st="$(git_prompt_info 2> /dev/null)"

  echo -n "%(?..%{$fg[red]%}%? ↵%{$reset_color%}    )"
  
  echo -n "$my_gray"
  if [ $git_st ]; then
    echo -n $git_st
  else
    echo -n "%n@%m"
  fi
  
  echo -n "%{$reset_color%}%"
}

# primary prompt
PROMPT='$my_gray------------------------------------------------------------%{$reset_color%}
$FG[032]$(collapse_pwd) $FG[105]%(!.#.»)%{$reset_color%} '
PROMPT2='%{$fg[red]%}\ %{$reset_color%}'

# right prompt
RPROMPT='$(right_prompt)'

# git settings
ZSH_THEME_GIT_PROMPT_PREFIX=""
ZSH_THEME_GIT_PROMPT_CLEAN="$my_green •"
ZSH_THEME_GIT_PROMPT_DIRTY="$my_orange *"
ZSH_THEME_GIT_PROMPT_SUFFIX=""

ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=7"

# window title
autoload -U add-zsh-hook

add-zsh-hook precmd  title_precmd
add-zsh-hook preexec title_preexec

title_precmd () {
  echo -n "\033];$(collapse_pwd)\007"
}

title_preexec () {
  emulate -L zsh
  setopt extended_glob
  local CMD=${1[(wr)^(*=*|sudo|ssh|-*)]} #cmd name only, or if this is sudo or ssh, the next cmd
  echo -n "\033];$(collapse_pwd) - $CMD\007"
}
