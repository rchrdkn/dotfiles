# dotfiles

My personal collection of dotfiles.

## Requirements

Make sure [oh my zsh](http://ohmyz.sh/) is installed.

```sh
$ curl -L http://install.ohmyz.sh | sh
```

## Install

Clone the repo into `~/.dotfiles` and run the install script.

```sh
$ git clone git://github.com/rchrdkn/dotfiles.git ~/.dotfiles
$ cd ~/.dotfiles
$ chmod gu+x ./tools/install.sh
$ ./tools/install.sh
```
