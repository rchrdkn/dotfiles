# symlink the .zshrc
ln -sfFv ~/.dotfiles/zsh/.zshrc ~/.zshrc
# symlink the .ackrc
ln -sfFv ~/.dotfiles/ack/.ackrc ~/.ackrc
# symlink the .atom directory
ln -sfFv ~/.dotfiles/atom ~/.atom
# symlink the .vimrc
ln -sfFv ~/.dotfiles/vim/.vimrc ~/.vimrc
# symlink the .gitignore_global and .gitconfig files
ln -sfFv ~/.dotfiles/git/.gitconfig ~/.gitconfig
ln -sfFv ~/.dotfiles/git/.gitignore_global ~/.gitignore_global
# setup term colours and settings (only on osx)
if [[ "$OSTYPE" == "darwin"* ]]; then
open ~/.dotfiles/term/Solarized\ Dark.terminal
defaults write com.apple.Terminal "Default Window Settings" "Solarized Dark"
defaults write com.apple.Terminal "Startup Window Settings" "Solarized Dark"
fi
