execute pathogen#infect()

" ================ General Config ====================

set nocompatible        " be iMproved
set hidden              " Allow un-saved buffers in background

syntax on               " syntax highlight

set clipboard=unnamed   " yank to the system register (*) by default
set showmatch           " Cursor shows matching ) and }
set number              " Set line numbers

set mouse=a

" Change leader to a comma because the backslash is too far away
" That means all \x commands turn into ,x
" The mapleader has to be set before vundle starts loading all 
" the plugins.
let mapleader=","

" Insert matching pairs
inoremap ( ()<Left>
inoremap [ []<Left>
inoremap { {}<Left>
inoremap /* /**/<Left><Left>

" ================ Folds ============================

set foldmethod=indent   " fold based on indent
set foldnestmax=3       " deepest fold is 3 levels
set nofoldenable        " dont fold by default

" ================ Searching ========================

set incsearch           " incremental search
set hlsearch            " search highlighting
set ignorecase          " ignore case when searching
set smartcase           " ignore case if search pattern is all lowercase,case-sensitive otherwise

" ================ Indentation ======================

set expandtab           " tabs instead of spaces
set tabstop=4           " a tab is four spaces
set shiftwidth=4        " number of spaces to use for autoindenting
set smarttab            " insert tabs on the start of a line according to context

set autoindent          " auto indentation
set smartindent         " 
set copyindent          " copy the previous indentation on autoindenting

" ================ Scrolling ========================

set scrolloff=8         " Start scrolling when we're 8 lines away from margins
set sidescrolloff=15    " same as above, but for side scrolling
set sidescroll=1

